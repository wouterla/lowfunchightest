'use strict';
import Ingredient from './ingredient';
import {IngredientRepositoryMemory} from "./ingredient_repository_memory";
import {IngredientRepositoryFirestore} from "./ingredient_repository_firestore";

class IngredientService {

    constructor() {
        this.initStorage();
    }

    initStorage() {
        if (!this.ingredientStorage) {
            if (global.gConfig.ingredient_service === "memory") {
                this.ingredientStorage = new IngredientRepositoryMemory();
            } else {
                this.ingredientStorage = new IngredientRepositoryFirestore();
            }
        }
    }

    async findAll() {
        return await this.ingredientStorage.findAll();
    }

    async findById(id) {
        return await this.ingredientStorage.findById(id);
    }

    async add(ingredient) {
        return await this.ingredientStorage.add(ingredient);
    }

    async update(ingredient) {
        return await this.ingredientStorage.update(ingredient);
    }

    async setup(ingredients) {
        return await this.ingredientStorage.setup(ingredients);
    }
}

export {
    IngredientService
}
