'use strict';
import Ingredient from './ingredient';
import {IngredientService} from "./ingredient_service";
import shortid from 'shortid';

class IngredientRepositoryMemory {

    ingredients = [];

    async findAll() {
        return new Promise(resolve => { resolve(this.ingredients) });
    }

    async findById(id) {
        let ingredientFound = this.ingredients.filter(ingredient => (ingredient.id === id))
        if (ingredientFound.length > 0) {
            return new Promise(resolve => { resolve(ingredientFound[0]) });
        } else {
            throw Error("Ingredient with ID '" + id + "' not found");
        }
    }

    async add(ingredient) {
        if (!ingredient.id) {
            let shorty = shortid.generate();
            ingredient.id = shorty;
        }
        this.ingredients.push(ingredient);
        return ingredient;
    }

    async update(ingredient) {
        let current = this.findById(ingredient.id);
        if (!current) {
            return false; // TODO: throw error
        }
        current.net_carbs = ingredient.net_carbs;
        return current;
    }

    async setup(data) {
        for(let i = 0; i < data.length; i++) {
            this.ingredients.push(data[i]);
        }
    }

}

export {
    IngredientRepositoryMemory
}
