'use strict';
import Ingredient from './ingredient';
import {IngredientService} from "./ingredient_service";
import Firestore from '@google-cloud/firestore';
import MockFirebase from 'mock-cloud-firestore';

class IngredientRepositoryFirestore {

    constructor() {
        this.loadFirestoreInstance();
        this.ingredient_collection = this.db.collection('ingredients');
    }

    loadFirestoreInstance() {
        if (!this.db) {
            if (global.gConfig.data_mode === "mocked") {
                const firebase = new MockFirebase({});
                this.db = firebase.firestore();
            } else {
                this.db = new Firestore({
                    projectId: 'lowfunchightest',
                    keyFilename: global.gConfig.keyFilename,
                });
            }
        }
        return this.db;
    }

    async findAll() {
        let ingredients = this.ingredient_collection.get()
            .then(snapshot => {
                let ingredientsFound = []
                snapshot.forEach(doc => {
                    ingredientsFound.push(doc.data());
                });
                return ingredientsFound;
            })
            .catch(err => {
                console.log('Error getting documents', err);
                throw err;
            });
        return ingredients;
    }

    async findById(id) {
        let ingredient = await this.ingredient_collection.doc(id);
        let ingredientFound = await ingredient.get()
            .then(ingredient => {
                if (!ingredient.exists) {
                    console.log('No such document!');
                } else {
                    return ingredient.data();
                }
            })
            .catch(err => {
                console.log('Error getting document', err);
                throw Error("Ingredient with ID '" + id + "' not found");
            });

        return ingredientFound;
    }

    async add(ingredient) {
        let data = JSON.stringify(ingredient);
        let resolvedingredient = await Promise.resolve(ingredient);
        console.log('adding ingredient: ' + data)

        let ref = await this.ingredient_collection.doc();
        let stored = await ref.set(JSON.parse(data));
        ingredient.id = ref.id;
        return ingredient;
    }

    async update(ingredient) {
        let stored = await this.ingredient_collection.doc(ingredient.id).set(ingredient, { merge: true });
        return stored;
    }

    async setup(data) {
        for(let i = 0; i < data.length; i++) {
            console.log('adding setup: ' + JSON.stringify(data[i]));
            let ingredient = JSON.stringify(data[i]);
            let resolvedingredient = await Promise.resolve(ingredient);

            let ref = await this.ingredient_collection.doc(data[i].id);
            let stored = await ref.set(data[i]);
        }
        return true;
    }


}

export {
    IngredientRepositoryFirestore
}
