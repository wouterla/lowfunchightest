import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import indexRouter from './routes/index';
import { IngredientService } from './ingredient_service';
import Firestore from "@google-cloud/firestore";

import { config } from './config';

const app = express();
const ingredientService = new IngredientService();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.use('/', indexRouter);

app.get("/api/ingredients", async function(req, res) {
    console.log('get all ingredients');
    await ingredientService.findAll().then(function(ingredients) {
        res.json(ingredients);
    }).catch(function(reason) {
        res.status(404);
        res.send(reason);
    });
    return res
});

app.get("/api/ingredients/:id", async function(req, res) {
    console.log('finding ingredient with id: ' + req.params.id);
    await ingredientService.findById(req.params.id).then(function(ingredient) {
        res.json(ingredient);
    }).catch(function(reason) {
        res.status(404);
        res.send(reason);
    });
    return res
});

app.put("/api/ingredients", async function(req, res) {
    console.log('put ingredient ' + JSON.stringify(req.headers));

    await ingredientService.add(req.body).then(function(ingredient) {
        console.log('put returning: ' + JSON.stringify(ingredient));
        res.status(201)
        res.json(ingredient);
    }).catch(function(reason) {
        console.log(reason);
        res.status(404);
        res.send(reason);
    });
    return res
});



// Should only be available to pact tests
app.post("/api/ingredients/setup", async function(req, res) {
    console.log('adding ingredients' + JSON.stringify(req.body));

    switch (req.body.state) {
        case 'jv1bzPVHTE':
            console.log('state used: ' + req.body.state);
            await ingredientService.setup([ { id: req.body.state, name: 'test', net_carbs: 5 } ]);
            break;
    }
    res.end()

    //
    // await ingredientService.setup(req.body).then(function(done) {
    //     res.status(201);
    // }).catch(function(reason) {
    //     res.status(500);
    //     res.send(reason);
    // });
});


app.post("/api/ingredients/:id", async function(req, res) {
    console.log('updating ingredient with id "' + req.params.id + '" to: ' + JSON.stringify(req.body));

    await ingredientService.update(req.body).then(function(ingredient) {
        res.json(ingredient);
    }).catch(function(reason) {
        res.status(404);
        res.send(reason);
    });
    return res
});


app.get("/api/version", function(req, res) {
    res.json({ version: "1.0" });
});

app.get("/status", function(req, res) {
    let connected = false;

    let db = new Firestore({
        projectId: 'lowfunchightest',
        keyFilename: global.gConfig.keyFilename,
    });
    if (db) {
        connected = true;
    }
    // TODO: Add a get/set to check credentials are sufficient
    let status = {
        "storage_engine": global.gConfig.ingredient_service,
        "data_mode": global.gConfig.data_mode,
        "db_reached": connected
    }
    res.json(status);
});

export default app;
