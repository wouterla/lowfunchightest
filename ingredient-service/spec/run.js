import Jasmine from 'jasmine'

const jasmine = new Jasmine();
const config = require('../src/config.js');
jasmine.loadConfigFile('spec/support/jasmine.json');
jasmine.execute();
