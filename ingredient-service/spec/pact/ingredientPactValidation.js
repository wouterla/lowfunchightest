import http from "http";

const Verifier = require('@pact-foundation/pact').Verifier
const path = require('path')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)

const config = require('../../src/config.js');

global.gConfig.node_port = 9191;

const { server, app } = require('../../src/bin/www.js')

app.post('/setup', (req, res) => {
    console.log('setup')
    switch (req.body.state) {
        case 'jv1bzPVHTE':
            console.log('state used');
            //ingredientService.setup([ { id: req.body.state, name: 'test', net_carbs: 5 } ]);
            break
    }

    res.end()
})

// Verify that the provider meets all consumer expectations
describe('Pact Verification', () => {
    it('should validate the expectations of Recipe Service', () => {
        let opts = {
            provider: 'ingredient_service',
            providerBaseUrl: 'http://localhost:9191/',
            providerStatesSetupUrl: 'http://localhost:9191/api/ingredients/setup',
            pactBrokerUrl: 'http://localhost:9292/',
            tags: ['prod', 'test'],
            pactBrokerUsername: 'postgres',
            pactBrokerPassword: 'password',
            publishVerificationResult: false,
            providerVersion: '1.0.0',
            // logLevel: "DEBUG",
        }

        return new Verifier(opts).verifyProvider().then(output => {
            console.log('Pact Verification Complete!')
            console.log(output)
        })
    })
})
