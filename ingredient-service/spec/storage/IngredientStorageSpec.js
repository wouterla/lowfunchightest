import Ingredient from '../../src/ingredient';
import {IngredientRepositoryMemory} from "../../src/ingredient_repository_memory";
import {IngredientRepositoryFirestore} from "../../src/ingredient_repository_firestore";

describe("IngredientStorage", function() {

    let ingredient;
    let ingredientStorage;

    beforeEach(function() {
        ingredient = new Ingredient();
        ingredient.name = "Test";
        ingredient.net_carbs = 10;
        ingredient.id = '1';

        if (global.gConfig.ingredient_service === "memory") {
            ingredientStorage = new IngredientRepositoryMemory();
            console.log("using in-memory storage");
        } else {
            ingredientStorage = new IngredientRepositoryFirestore();
            console.log("using firebase");
        }
    });

    it("should be able to store a ingredient", async function() {
        let stored = await ingredientStorage.add(ingredient);
        let id = stored.id;
        console.log('id: ' + JSON.stringify(id))

        await ingredientStorage.findById(id).then(function(ingredientFound) {
            expect(ingredientFound.net_carbs).toEqual(10);
        });
    });

    it("should be able to retrieve all stored ingredients", function() {
        console.log('findall test');
        ingredientStorage.findAll().then(function(ingredients) {
            ingredients.forEach(function(ingredient) {
                console.log("ingredient: " + ingredient);
            });
        });
    });

});
