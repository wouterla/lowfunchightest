import Ingredient from '../../src/ingredient';

describe("Ingredient", function() {
    let ingredient;

    beforeEach(function() {
        // test
        ingredient = new Ingredient();
    });

    it("return the carbs in the ingredient", function() {
        let recipe = new Ingredient();
        expect(recipe.totalCarbs()).toEqual(10);
    });
});
