#!/usr/bin/env bash

# gcloud auth login
# gcloud components install kubectl

export PROJECT_ID=lowfunchightest

# Set config
gcloud config set project lowfunchightest
gcloud config set compute/zone europe-west4-a

# Create cluster
gcloud container clusters create lowfunchightest
gcloud container clusters get-credentials lowfunchightest

# Create namespace(s)
# TODO: Add sed commands to create more environments
kubectl create -f k8s/namespace.json

# Create service account
gcloud beta iam service-accounts create "lowfunchightest-ci" \
    --description "CI user for lowfunchightest" \
    --display-name "lowfunchightest-ci"

# Get service account id
SERVICE_ACCOUNT_NAME=$(gcloud iam service-accounts list \
  --filter name:"lowfunchightest-ci" \
  --format="value(name.basename())")

# Give service account access to k8s
gcloud projects add-iam-policy-binding "lowfunchightest" \
  --member serviceAccount:$SERVICE_ACCOUNT_NAME \
  --role roles/iam.serviceAccountUser

gcloud projects add-iam-policy-binding "lowfunchightest" \
  --member serviceAccount:$SERVICE_ACCOUNT_NAME \
  --role=roles/container.clusterAdmin

gcloud projects add-iam-policy-binding "lowfunchightest" \
  --member serviceAccount:$SERVICE_ACCOUNT_NAME \
  --role=roles/storage.admin

# Create and retrieve private key for service account to configure in gitlab
gcloud iam service-accounts keys create ~/key.json
  --iam-account lowfunchightest-ci@lowfunchightest.iam.gserviceaccount.com

# Create gitlab account (to run gitlab runners)
kubectl apply -f k8s/gitlab-admin-service-account.yaml

# Get token to configure in gitlab
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')

#################################################################
# Firestore
#
# Created db instance through the web console
# Now add account
#################################################################
export DATA_ACCOUNT="lowfunchightest-firestore"
gcloud iam service-accounts create ${DATA_ACCOUNT}
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${DATA_ACCOUNT}@${PROJECT_ID}.iam.gserviceaccount.com" \
  --role "roles/owner"
gcloud iam service-accounts keys create ${DATA_ACCOUNT}.json --iam-account ${DATA_ACCOUNT}@${PROJECT_ID}.iam.gserviceaccount.com

# make key available to application (also add in CI pipeline)
export GOOGLE_APPLICATION_CREDENTIALS=$(pwd)/${DATA_ACCOUNT}.json
