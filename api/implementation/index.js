const express = require("express");
const app = express();
const port = 3000;
let bodyParser = require("body-parser");

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use(function(req, res, next) {
  const key = req.get("lowfunchightest-key");
  res.set("X-Clacks-Overhead", "GNU Terry Pratchet");
  if (key) next();
  //todo: Do some key verification here
  else res.send(403);
});

const recipes = [
  {
    name: "Very tasty slow carb lunch possibility",
    difficulty: 3,
    description:
      "Fusing the best of meat and chicken with some vegetables along the side. You will **love** it.",
    ingredients: ["Beef", "Chicken", "Onions", "Soup"],
    meal: "Dinner",
    steps: [
      {
        duration: 3,
        description:
          "Chop the onions very fine. Make sure they are really tiny. You could put them in a blender however that gives them a metallic taste"
      }
    ],
    id: "e96df161-c336-4594-89ed-8c49bfca327b"
  }
];

app.get("/", function(req, res) {
  res.json(recipes);
});

app.post("/ingredients", function(req, res) {
  res.json(req.body);
});

app.listen(port, () =>
  console.log(`lowfunchightest api proxy listening on port ${port}!`)
);
