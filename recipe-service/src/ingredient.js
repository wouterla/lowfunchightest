'use strict';

class Ingredient {

    constructor(name, net_carbs) {
        this.name = name;
        this.net_carbs = net_carbs;
    }

    totalCarbs() {
        return 10;
    }
}

export default Ingredient;
