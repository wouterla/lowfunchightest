import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import indexRouter from './routes/index';
import Firestore from "@google-cloud/firestore";

import { config } from './config';

import { RecipeService } from './recipe_service';
import request from "superagent";

const app = express();
const recipeService = new RecipeService();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.use('/', indexRouter);

app.get("/api/recipes", async function(req, res) {
    await recipeService.findAll().then(function(recipes) {
        res.json(recipes);
    });
    return res;
});

app.get("/api/recipes/:id", async function(req, res) {
    console.log('finding recipe with name: ' + req.params.id);
    await recipeService.findByName(req.params.id).then(function(recipe) {
        res.json(recipe);
    }).catch(function(reason) {
        res.status(404);
        res.send(reason);
    });
    return res;
});

app.get("/api/version", function(req, res) {
    res.json({ version: "1.0" });
});

app.get("/status", async function(req, res) {
    let connected = false;

    let db = new Firestore({
        projectId: 'lowfunchightest',
        keyFilename: global.gConfig.keyFilename,
    });
    // TODO: Add actual access to check credentials
    if (db) {
        connected = true;
    }

    // ingredient service
    let ingredient_service_status = await request.get(global.gConfig.ingredient_service + '/api/ingredients').then(res => res.status);

    let status = {
        "storage_engine": global.gConfig.recipe_service,
        "data_mode": global.gConfig.data_mode,
        "db_reached": connected,
        "ingredient_service_endpoint": global.gConfig.ingredient_service,
        "ingredient_service_status": ingredient_service_status
    }


    res.json(status);
});

export default app;
