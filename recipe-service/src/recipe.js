'use strict';
import Ingredient from './ingredient';

class Recipe {
    ingredients = [];

    constructor() {
        this.name;
        this.net_carbs;
    }

    calculateTotalCarbs() {
        let totalCarbs = this.getIngredients().reduce(function(total, ingredient) {
            return total + ingredient.net_carbs;
        }, 0);
        return totalCarbs;
    }

    addIngredient(ingredient) {
        this.ingredients.push(ingredient);
    }

    getIngredients() {
        return this.ingredients;
    }

    persist() {
        let ingredientIds = this.ingredients.map(function(ingredient) { return ingredient.id });
        let persistVersion = {};
        persistVersion.name = this.name;
        if (this.net_carbs) { persistVersion.net_carbs = this.net_carbs; }
        persistVersion.ingredients = ingredientIds;
        return persistVersion;
    }
}

export default Recipe;
