'use strict';
import Recipe from './recipe';
import {RecipeService} from "./recipe_service";
import Firestore from '@google-cloud/firestore';
import MockFirebase from 'mock-cloud-firestore';

class RecipeRepositoryFirestore {

    constructor() {
        this.loadFirestoreInstance();
        this.recipe_collection = this.db.collection('recipes');
    }

    loadFirestoreInstance() {
        if (!this.db) {
            if (global.gConfig.data_mode === "mocked") {
                const firebase = new MockFirebase({});
                this.db = firebase.firestore();
                // console.log("using mocked firebase");
            } else {
                this.db = new Firestore({
                    projectId: 'lowfunchightest',
                    keyFilename: global.gConfig.keyFilename,
                });
                // console.log("using persisted firebase");
            }
        }
        return this.db;
    }

    async findAll() {
        let recipes = this.recipe_collection.get()
            .then(snapshot => {
                let recipesFound = []
                snapshot.forEach(doc => {
                    recipesFound.push(doc.data());
                });
                return recipesFound;
            })
            .catch(err => {
                console.log('Error getting documents', err);
                throw err;
            });
        return recipes;
    }

    async findByName(recipeName) {
        let recipe = this.recipe_collection.doc(recipeName);
        let recipeFound = recipe.get()
            .then(recipe => {
                if (!recipe.exists) {
                    console.log('No such document!');
                } else {
                    return recipe.data();
                }
            })
            .catch(err => {
                console.log('Error getting document', err);
                throw Error("Recipe with name '" + name + "' not found");
            });

        return recipeFound;
    }

    async add(recipe) {
        let data = JSON.stringify(recipe);
        let resolvedRecipe = await Promise.resolve(recipe);
        this.recipe_collection.doc(resolvedRecipe.name).set(JSON.parse(data));
    }

    async update(recipe) {
        await this.recipe_collection.doc(recipe.name).set(recipe, { merge: true });
        return true;
    }

}

export {
    RecipeRepositoryFirestore
}
