'use strict';
import express from 'express';
import request from 'superagent';
import Ingredient from './ingredient';

const server = express();

const apiEndpoint = global.gConfig.ingredient_service;

class IngredientGateway {

    constructor() {
    }

    async findAll() {
        return request.get(apiEndpoint + '/api/ingredients').then(res => res.body);
    }

    async add(ingredient) {
        return request.put(apiEndpoint + '/api/ingredients')
            .send({ name: ingredient.name, net_carbs: ingredient.net_carbs }).then(res => res.body);
    }

    async update(ingredient) {
        return request.post(apiEndpoint + '/api/ingredients/' + ingredient.id)
            .send({ name: ingredient.name, net_carbs: ingredient.net_carbs }).then(res => res.body);
    }

    async get(id) {
        return request.get(apiEndpoint + '/api/ingredients/' + id).then(res => res.body)
    }
}

export {
    IngredientGateway
}
