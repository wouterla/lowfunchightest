'use strict';
import Recipe from './recipe';
import {RecipeService} from "./recipe_service";

class RecipeRepositoryMemory {

    recipes = [];

    async findAll() {
        return new Promise(resolve => { resolve(this.recipes) });
    }

    async findByName(recipeName) {
        let recipeFound = this.recipes.filter(recipe => (recipe.name === recipeName))
        if (recipeFound.length > 0) {
            return new Promise(resolve => { resolve(recipeFound[0]) });
        } else {
            return null;
        }
    }

    async add(recipe) {
        this.recipes.push({ name: recipe.name, net_carbs: recipe.net_carbs });
    }

    async update(recipe) {
        let current = await this.findByName(recipe.name);
        let index = this.recipes.indexOf(current);
        if (!current) {
            return false; // TODO: throw error
        }
        this.recipes[index] = recipe;
        return true;
    }
}

export {
    RecipeRepositoryMemory
}
