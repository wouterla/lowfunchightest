'use strict';
import Recipe from './recipe';
import Ingredient from './ingredient';
import { IngredientGateway } from './ingredient_gateway';
import { RecipeRepositoryMemory } from "./recipe_repository_memory";
import { RecipeRepositoryFirestore } from "./recipe_repository_firestore";

class RecipeService {

    constructor() {
        this.initStorage();
    }

    initStorage() {
        if (!this.recipeStorage) {
            if (global.gConfig.recipe_service === "memory") {
                this.recipeStorage = new RecipeRepositoryMemory();
                // console.log("using in-memory storage");
            } else {
                this.recipeStorage = new RecipeRepositoryFirestore();
                // console.log("using firebase");
            }
        }
        this.ingredientAdapter = new IngredientGateway();
    }

    async findAll() {
        let recipeData = await this.recipeStorage.findAll();

        let allRecipes = await this.convertAllToObjects(recipeData);
        return allRecipes;
    }

    async findByName(recipeName) {
        let recipeData = await this.recipeStorage.findByName(recipeName);
        let recipeObj = await this.convertDataToObject(recipeData);
        return recipeObj;
    }

    async add(recipe) {
        recipe.ingredients = await this.syncIngredients(recipe.getIngredients());
        return await this.recipeStorage.add(recipe.persist());
    }

    async update(recipe) {
        let ingredients = await this.syncIngredients(recipe.getIngredients());
        recipe.ingredients = ingredients;
        return await this.recipeStorage.update(recipe.persist());
    }

    async convertDataToObject(recipeData) {
        let recipeObj = new Recipe();
        recipeObj.name = recipeData.name;
        recipeObj.net_carbs = recipeData.net_carbs;
        if (recipeData.ingredients) {
            recipeObj.ingredients = await this.loadIngredients(recipeData.ingredients);
        }
        return recipeObj;
    }

    async convertAllToObjects(recipeData) {
        let that = this;
        let allRecipes = [];
        for (let i = 0; i < recipeData.length; i++) {
            let entry = recipeData[i];
            allRecipes.push(await that.convertDataToObject(entry));
        }
        return allRecipes;
    }

    async loadIngredients(ingredientIds) {
        let ingredients = [];
        for (let i = 0; i < ingredientIds.length; i++) {
            let id = ingredientIds[i];
            let ingredientData = await this.ingredientAdapter.get(id);
            let ingredientObj = new Ingredient(ingredientData.name, ingredientData.net_carbs);
            ingredientObj.id = ingredientData.id;
            ingredients.push(ingredientObj);
        }
        return ingredients;
    }

    async syncIngredients(ingredients) {
        let stored = []
        for (let i = 0; i < ingredients.length; i++) {
            let ingredient = ingredients[i];
            if (ingredient.id) {
                let returnedIngredient = await this.ingredientAdapter.update(ingredient);
                stored.push(returnedIngredient);
            } else {
                let returnedIngredient = await this.ingredientAdapter.add(ingredient);
                ingredient.id = returnedIngredient.id;
                stored.push(ingredient);
            }
        }
        return stored;
    }
}

export {
    RecipeService
}
