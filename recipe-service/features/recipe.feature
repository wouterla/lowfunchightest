Feature: recipe
  In order to retrieve slow carb recipes
  As a hungry person
  I want to find something to eat

  Scenario: Find a recipe
    Given A recipe "Fat Bombs" is available
    When I get a list of recipes
    Then "Fat Bombs" is shown

  Scenario: Create a recipe
    Given a new recipe named "Fathead Pizza"
    When that recipe is submitted
    Then "Fathead Pizza" is available

  Scenario: List recipes
    Given the following recipes in the system
      | Recipe Name   | Net Carbs |
      | Fat Bombs     | 7         |
      | Fathead Pizza | 5         |
    When a list of all recipes is requested
    Then that list includes
      | Recipe Name   |
      | Fathead Pizza |
      | Fat Bombs     |

  Scenario: Update recipe
    Given the following recipes in the system
      | Recipe Name   | Net Carbs |
      | Fat Bombs     | 5         |
    When we change the Net Carbs for "Fat Bombs" to "7"
    Then the recipe "Fat Bombs" has "7" of Net Carbs

  Scenario: Add Ingredients
    Given A recipe "Fat Bombs" is available
    When we add these ingredients to "Fat Bombs"
      | Ingredient Name   | Net Carbs |
      | Whipped Cream     | 3         |
      | Cocao             | 3         |
    Then the recipe "Fat Bombs" has ingredients
      | Ingredient Name   | Net Carbs |
      | Whipped Cream     | 3         |
      | Cocao             | 3         |
