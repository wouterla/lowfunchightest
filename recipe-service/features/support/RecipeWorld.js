import config from '../../src/config.js';

import Recipe from './../../src/recipe';
import { RecipeService } from './../../src/recipe_service';

const { setWorldConstructor } = require("cucumber");

class RecipeWorld {
    constructor() {
        this.recipeService = new RecipeService();
        this.recipe = new Recipe();
    }

    addRecipe(recipe) {
        this.recipe = recipe;
        this.recipeService.add(recipe)
    }

    updateRecipe(recipe) {
        return this.recipeService.update(recipe)
    }

    retrieveRecipes() {
        this.recipeList = this.recipeService.findAll();
    }

    getRecipe(recipeName) {
        this.recipe = this.recipeService.findByName(recipeName);
        return this.recipe;
    }

    isAvailable(recipeName) {
        this.recipe = this.recipeService.findByName(recipeName);
        if (this.recipe) { return true }

        return false;
    }
}

setWorldConstructor(RecipeWorld);
