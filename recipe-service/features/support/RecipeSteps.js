import Ingredient from "../../src/ingredient";

const {Given, When, Then, And, But} = require("cucumber");
const {expect} = require("chai");

import Recipe from './../../src/recipe';

Given("A recipe {string} is available", function (recipeName) {
    let recipe = new Recipe();
    recipe.name = recipeName;
    this.addRecipe(recipe)
});

Given(/^a new recipe named "([^"]*)"$/, function (recipeName) {
    let recipe = new Recipe();
    recipe.name = recipeName;
    this.addRecipe(recipe)
});

When("I get a list of recipes", function () {
    this.retrieveRecipes();
});

Then("{string} is shown", function (recipeName) {
    this.recipeList.then(function(all_recipes) {
        let expected = all_recipes.filter(recipe => (recipe.name === recipeName))
        expect(expected).not.to.be.undefined;
    })
});

When(/^that recipe is submitted$/, function () {
    // Nothing to do
});

Then(/^"([^"]*)" is available$/, function (recipeName) {
    expect(this.isAvailable(recipeName)).to.be.true;
});

Given(/^the following recipes in the system$/, function (table) {
    let rows =  table.rows();
    for(let i = 0; i < rows.length; i++) {
        let newRecipe = new Recipe();
        newRecipe.name = rows[i][0];
        newRecipe.net_carbs = rows[i][1];
        this.addRecipe(newRecipe);
    }
});

When(/^a list of all recipes is requested$/, function () {
    this.retrieveRecipes();
});

Then(/^that list includes$/, function (table) {
    let rows =  table.rows();
    for(let i = 0; i < rows.length; i++) {
        let recipeName = rows[i][0];
        expect(this.isAvailable(recipeName)).to.be.true;
    }
});

When(/^we change the Net Carbs for "([^"]*)" to "([^"]*)"$/, async function (recipeName, net_carbs) {
    // let that = this;
    // this.getRecipe(recipeName).then(function(recipe) {
    //     recipe.net_carbs = net_carbs;
    //     that.updateRecipe(recipe);
    // });
    let that = this;
    let recipe = await this.getRecipe(recipeName);
    recipe.net_carbs = net_carbs;
    let update = await this.updateRecipe(recipe);
});

Then(/^the recipe "([^"]*)" has "([^"]*)" of Net Carbs$/, function (recipeName, net_carbs) {
    this.getRecipe(recipeName).then(function(recipe) {
        expect(recipe.net_carbs).to.equal(net_carbs);
    });
});

When('we add these ingredients to {string}', async function (recipeName, table) {
    let recipe = await this.getRecipe(recipeName);
    let rows =  table.rows();
    let ingredients = []
    for(let i = 0; i < rows.length; i++) {
        let newIngredient = new Ingredient();
        newIngredient.name = rows[i][0];
        newIngredient.net_carbs = rows[i][1];
        recipe.addIngredient(newIngredient);
    }
    let stored = await this.updateRecipe(recipe);
});

Then('the recipe {string} has ingredients', async function (recipeName, table) {
    let recipe = await this.getRecipe(recipeName);
    let ingredientNames = recipe.getIngredients().map(function(ingredient) {
        return ingredient.name;
    });
    let rows =  table.rows();
    for(let i = 0; i < rows.length; i++) {
        let ingredientName = rows[i][0];
        expect(ingredientNames).to.contain(ingredientName);
    }
});
