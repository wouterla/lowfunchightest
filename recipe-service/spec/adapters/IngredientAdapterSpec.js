import { IngredientGateway } from "../../src/ingredient_gateway";
import Ingredient from "../../src/ingredient";
import request from 'superagent';
import mocker from 'superagent-mocker';

var mock = mocker(request);
const apiEndpoint = global.gConfig.ingredient_service;

describe("IngredientGateway", function() {

    beforeEach(function() {
        mock.clearRoutes();
    });

    it("should get a response from the ingredient service", async function() {
        let ingredients = new IngredientGateway();
        mock.get(apiEndpoint + '/api/ingredients', function(req) {
            return {
                status: 200,
                body: [{name: 'test', net_carbs: '5'}]
            }
        });
       let result = await ingredients.findAll();
       expect(result).toEqual([ { name: 'test', net_carbs: '5' } ]);
    });

    it("should create a new ingredient with a name", async function() {
        let ingredients = new IngredientGateway();
        mock.put(apiEndpoint + '/api/ingredients', function(req) {
            return {
                status: 201,
                body: { id: '1', name: req.body.name, net_carbs: req.body.net_carbs }
            }
        });
        let ingredient = new Ingredient('test', 6);
        let result = await ingredients.add(ingredient);
        expect(result.name).toEqual(ingredient.name);
    });

    it("should update an ingredient", async function() {
        let ingredients = new IngredientGateway();
        mock.post(apiEndpoint + '/api/ingredients/:id', function(req) {
            return {
                status: 201,
                body: { id: req.params.id, name: req.body.name, net_carbs: req.body.net_carbs }
            }
        });
        let ingredient = new Ingredient('test', 6);
        ingredient.id = '1';
        let result = await ingredients.update(ingredient);
        expect(result.id).toEqual(ingredient.id);
    });

    it("should find an ingredient by id", async function() {
        let ingredients = new IngredientGateway();
        mock.get(apiEndpoint + '/api/ingredients/:id', function(req) {
            return {
                status: 200,
                body: { id: req.params.id, name: 'test', net_carbs: 6 }
            }
        });
        let result = await ingredients.get('1');
        expect(result.id).toEqual('1');
        expect(result.name).toEqual('test');
    });

});
