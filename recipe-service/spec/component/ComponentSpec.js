import request from 'supertest';
import express from 'express';
import app from '../../src/app.js' ;

describe('GET /api/recipes', function() {
    it('responds with json', function(done) {
        request(app)
            .get('/api/recipes')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});
