import provider from './pactSetup';
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const expect = chai.expect
chai.use(chaiAsPromised)

const { somethingLike: like, term } = require('@pact-foundation/pact').Matchers
import request from 'superagent';
const apiEndpoint = "http://127.0.0.1:" + global.port;

var ingredientOne =
    {
        id: 'jv1bzPVHTE',
        name: like('test'),
        net_carbs: like(3)
    };
var ingredientTwo =
    {
        id: like('hW1OHaxRw'),
        name: 'test2',
        net_carbs: 3
    };

describe('Pact with Ingredient Service', function() {
    describe('given ingredients in the dataset', function() {
        describe('when we retrieve one specific ingredient', function() {
            before(function() {
                return global.provider.setup()
                    .then(function() {
                        global.provider.addInteraction({
                            state: 'jv1bzPVHTE',
                            params: ingredientOne,
                            uponReceiving: 'a request for a specific ingredient',
                            withRequest: {
                                method: 'GET',
                                path: '/api/ingredients/' + ingredientOne.id,
                            },
                            willRespondWith: {
                                status: 200,
                                body: {
                                    id: 'jv1bzPVHTE',
                                    name: like('test'),
                                    net_carbs: like(3)
                                }
                            }
                        })
                    })
            });

            it('can get an ingredient by id', async function() {
                let id = ingredientOne.id;
                let response = await request.get(apiEndpoint + '/api/ingredients/' + id);

                let obj = JSON.parse(response.text);

                expect(obj).to.have.property("name", 'test');
                expect(obj).to.have.property('net_carbs', 3);

            });

            it('should validate the interactions and create a contract', function() {
                return global.provider.verify()
            })
        });

        describe('when we add a new ingredient', function() {
            before(function() {
                return global.provider.addInteraction({
                            uponReceiving: 'a new ingredient',
                            withRequest: {
                                method: 'PUT',
                                path: '/api/ingredients',
                                body: { name: 'test2', net_carbs: 3 },
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                            },
                            willRespondWith: {
                                status: 201,
                                body: {
                                    id: like('hW1OHaxRw'),
                                    name: 'test2',
                                    net_carbs: 3
                                }
                            }
                        })
            });

            it('can add a new ingredient', async function() {
                let response = await request.put(apiEndpoint + '/api/ingredients')
                    .send({ name: ingredientTwo.name, net_carbs: ingredientTwo.net_carbs })
                    .then(res => res);

                let obj = JSON.parse(response.text);

                expect(obj.id).not.to.be.undefined;
                expect(obj).to.have.property("name", 'test2');
                expect(obj).to.have.property('net_carbs', 3);
            });

            it('should validate the interactions and create a contract', function() {
                return global.provider.verify()
            })
        });

        // Write pact files to file
        after(function() {
            return global.provider.finalize()
        });
    });
});
