import path from 'path';
import { Pact } from '@pact-foundation/pact';

import { config } from '../../src/config';

global.port = 9898;
global.provider = new Pact({
    port: global.port,
    log: path.resolve(process.cwd(), 'logs', 'mockserver-integration.log'),
    dir: path.resolve(process.cwd(), 'pacts'),
    spec: 2,
    cors: true,
    pactfileWriteMode: 'update',
    consumer: 'recipe_service',
    provider: 'ingredient_service'
});
