import Recipe from './../../src/recipe';
import {RecipeRepositoryMemory} from "../../src/recipe_repository_memory";
import {RecipeRepositoryFirestore} from "../../src/recipe_repository_firestore";

describe("RecipeStorage", function() {

    let recipe;
    let recipeStorage;

    beforeEach(function() {
        recipe = new Recipe();
        recipe.name = "Test";
        recipe.net_carbs = 10;

        if (global.gConfig.recipe_service === "memory") {
            recipeStorage = new RecipeRepositoryMemory();
            // console.log("using in-memory storage");
        } else {
            recipeStorage = new RecipeRepositoryFirestore();
            // console.log("using firebase");
        }
    });

    it("should be able to store a recipe", function() {
        recipeStorage.add(recipe);

        recipeStorage.findByName("Test").then(function(recipeFound) {
            expect(recipeFound.net_carbs).toEqual(10);
        });
    });

    it("should be able to retrieve all stored recipes", function() {
        recipeStorage.findAll().then(function(recipes) {
            recipes.forEach(function(recipe) {
                console.log("recipe: " + recipe);
            });
        });
    });

});
