import Recipe from './../../src/recipe';
import Ingredient from './../../src/ingredient';

describe("Recipe", function() {
    let recipe;

    beforeEach(function() {
        // test
        recipe = new Recipe();
    });

    it("should calculate the sum of all ingredient carbs", function() {
        let recipe = new Recipe();
        recipe.addIngredient(new Ingredient('test1', 4));
        recipe.addIngredient(new Ingredient('test2', 4));
        expect(recipe.calculateTotalCarbs()).toEqual(8);
    });

    describe("Ingredients", function() {
        it("should add an ingredient", function() {
            let recipe = new Recipe();
            let ingredient = new Ingredient('test', 4);
            recipe.addIngredient(ingredient);
            expect(recipe.getIngredients().length).toEqual(1);
        })
    })
});
